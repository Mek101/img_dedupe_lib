 1 acquire cache lock
 2 start cache transaction
 3 check if the color of the cache is marked in white or black
 4 check if the sign cache of A is present
	5 if not, generate the sign of A and update the cache
 6 mark the cache of A of the opposite color
~~ 7 check if the sign cache of B is present ~~
 	8 if not, generate the sign of B and update the cache
 9 mark the cache of B of the opposite color
 10 check in cache if there's a relationship between A and B
	11 if not, generate it and update the cache
 12 for each cache element with the current/initial color, check if the file still exists
 	13 if not, delete the item from the cache.
 14 commit transaction
 15 release cache lock
