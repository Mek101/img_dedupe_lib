1 different views as filters on the graph

Components:
The comparator -> creates a relationship between 2 images
The collection comparator -> generates the relationships in a collection of images, this results in a web of relationship, a graph





- Node
	Processed abstraction over the image.
	Has a sign
	Has all the attributes of the image
	- Image:
		Identified by it's path.
		Has a format.
		Has a resolution.
		Has a file size.
		Consistency with the cache is verifiable with an attribute (timestap? hash? mix of one of the latter with the other attributes?).

- GraphBuilder:
	Given a collection of images and configuration (with the cache), builds and outputs a graph with the connected updated cache.
	
	- Signer
		Ensures each image has a sign, using the cache to speed up operations; generating a sign and updating the cache if cache-miss occurs.
		- SignGenerator
			Generates a new sign for each given image.
			Shall be multithreaded.
		- SignCacheManager:
			Stores and manages the chace (which could be implemented in a db).
				- Adapter
					Connects the SignCacheManager to the cache database implementation, providing a basic abstraction for better portability.
					Handles filesystem case-sensitivity.
			
			If an image isn't present in the input
			- mode A: check in the filesystem if it still exist, if it doesn't, discard it from the cache.
			- mode B: discard it from the cache.
		
			If an image has changed (hash? timestap of last modify?), remove it from the cache.
			Once a graph has been generated, save it as cache for the next run. 
		
	- Comparator:
		Compares every image given as input with each other.
		When asked for it's output, it outputs a weighted graph.
		Shall be multithreaded.

- ~~View:~~
	~~Contains a single rule, a predicate.~~
	~~Given a graph, applies the predicate on it (the whole graph? a single item like an image or relationship?), outputting a new sub-graph (a subset of the input graph).~~

- ~~Selector:~~
	~~Given a weighted graph and an ordered list of views, outputs a view of the graph (a subgraph of it).~~

Issues:
	As an optimization, a first view should be applied to the collection of images, removing images from excluded paths. -> Use it as different things: one is a filter applied before the GraphBuilder that *removes* the image from even being considered, the other is a view which lowers the rank of images which matches.
	It may happen that some duplicates of the same image, none of them respects all of the views, therefore they're all marked for elimination. This shouldn't happen. Perhaps a system where each view simply ranks (adding or removing points) the images would be better. It's also possible to customize the weight of each rule.
	If an image has no duplicates, it should be discarded from the graph.
	

- Filter:
	- Splitter:
		Given an input graph and a threshold, splits the input graph into collections of duplicated images by eliminating the arcs of the graph whose weight is bellow the threshold; it then discards the images with no duplicates, and outputs a series of collections of duplicates.

	- Ranker:
		Given a collection of predicates and a collection of duplicate images, applies each predicate to each image, outputting a list of images ordered according to the points assigned by the predicates.

Issues:
	Since the Filter employs a 2 stage filtering method, it's impossible to re-adjust the rules without re-running it from scratch.
	The user may want to manually update the cache if some items are deleted from the output graph.
