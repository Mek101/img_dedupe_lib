There are at least 3 possible candidates to provide a way to calculate the difference between different images:

- Image Magick rust bindings
	https://lib.rs/crates/magick_rust
	Requires to install Image Magick.
	Already used: not really fast.
	Bloated asf for this project.

- RGBA Structural Similarity
	https://github.com/kornelski/dssim
	Focused for this usage.
	Could be more accurate (maybe?).
	Too complex to understand.
	The output values go from 0 to infinite: therefore it's impossible to determinate a range in with an upper threshold.

- img_hash *(chosed!)*
	https://github.com/abonander/img_hash
	Focused for this usage.
	Simple asf to use.
	Need to undertand how to handle the resulting hamming distance.
	Depends on the `image` crate, which must be added manually.


Cache: maybe use a sqlite database?