mod filter;
mod graphconstructor;
mod wrappers;

use std::path::Path;
use std::sync::Arc;

use img_hash::HashAlg;
use petgraph::{Graph, Undirected};
use rayon::{ThreadPool, ThreadPoolBuilder};

use crate::wrappers::cachemanager::{CacheManager, CacheMode};
use crate::wrappers::filedata::FileData;

// Public exports
pub use crate::wrappers::Distance;

pub type ImageGraph = Graph<Arc<FileData>, Distance, Undirected>;

/// A handler to keep and re-use resources that would need to be re-initialized every time otherwise.
pub struct Deduper {
    pool: ThreadPool,
    cache_manager: Arc<CacheManager>,
}

impl Deduper {
    pub fn new(parallelism: usize, cache_path: &Path, cache_mode: CacheMode) -> Self {
        assert!(parallelism > 0, "Cannot have zero parallelism!");

        Self {
            pool: ThreadPoolBuilder::new().num_threads(parallelism).build().unwrap(),
            cache_manager: Arc::new(CacheManager::new(cache_path, cache_mode)),
        }
    }

    pub fn graph_images<'a>(
        &self,
        images: impl IntoIterator<Item = &'a Arc<FileData>> + Send,
        hash_alg: HashAlg,
    ) -> Box<ImageGraph> {
        graphconstructor::build_graph(images, hash_alg, &self.cache_manager, &self.pool)
    }

    pub fn split_image_graph(
        &self,
        graph: &ImageGraph,
        tolerance_threshold: u32,
        discard_unique: bool,
    ) -> Vec<Vec<Arc<FileData>>> {
        filter::split_to_groups(graph, tolerance_threshold, discard_unique, &self.pool)
    }

    pub fn rank<P>(&self, images: &[Vec<Arc<FileData>>], predicates: &[P]) -> Vec<Vec<(Arc<FileData>, i32)>>
    where
        P: Sync + Fn(&FileData) -> i32 + Send,
    {
        filter::rank(images, predicates, &self.pool)
    }

    pub fn dedupe_images<'a, P>(
        &self,
        images: impl IntoIterator<Item = &'a Arc<FileData>> + Send,
        hash_alg: HashAlg,
        tollerance_threshold: u32,
        discard_unique: bool,
        predicates: &[P],
    ) -> Vec<Vec<(Arc<FileData>, i32)>>
    where
        P: Sync + Fn(&FileData) -> i32 + Send,
    {
        let graph = self.graph_images(images, hash_alg);
        let groups = self.split_image_graph(graph.as_ref(), tollerance_threshold, discard_unique);
        self.rank(&groups, predicates)
    }
}

#[cfg(test)]
mod tests {
    pub mod utils {
        use std::fs;
        use std::path::PathBuf;
        use std::sync::Arc;

        use glob;
        use img_hash::HashAlg;
        use num_cpus;
        use once_cell::sync::Lazy;
        use rayon::{ThreadPool, ThreadPoolBuilder};

        use crate::wrappers::cachemanager::{CacheManager, CacheMode};
        use crate::wrappers::filedata::FileData;

        /// Path to the directory containing the images for the tests.
        const TEST_IMAGES_PATH: Lazy<PathBuf> = Lazy::new(|| {
            let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
            path.push("resources/tests/images/");
            path
        });

        /// The number of unique images (and therefore groups).
        pub const TEST_IMAGES_UNIQUE_COUNT: usize = 2;

        /// Vector with the groups of images that should be idientifies as duplicates.
        pub const TEST_IMAGES_CORRECT_MATCHES: Lazy<Vec<Vec<PathBuf>>> = Lazy::new(|| {
            // All the groups basenames as patterns.
            let groups_basenames =
                vec!["ani_girl_1*", "fist_*", "landscape_1*", "landscape_4*", "unique_1.jpg", "unique_2.jpg"];

            // Collects each group in it's own vector.
            let mut groups = groups_basenames
                .iter()
                .map(|g| {
                    let mut p = TEST_IMAGES_PATH.clone();
                    p.push(g);
                    let pattern = p.to_str().unwrap();

                    let mut group = glob::glob(pattern).unwrap().map(|r| r.unwrap()).collect::<Vec<PathBuf>>();
                    group.shrink_to_fit();
                    group
                })
                .collect::<Vec<Vec<PathBuf>>>();
            groups.shrink_to_fit();
            groups
        });

        /// Image files to test on.
        pub const TEST_FILEDATA: Lazy<Vec<Arc<FileData>>> = Lazy::new(|| {
            fs::read_dir(TEST_IMAGES_PATH.as_path())
                .unwrap()
                .filter_map(|p| {
                    let path = p.unwrap().path();
                    let img_res = image::open(path.clone());
                    match img_res {
                        Ok(img) => Some(Arc::new(FileData::new(path.as_path(), img))),
                        Err(_) => None,
                    }
                })
                .collect()
        });

        pub const TEST_CACHE_MANAGER_NONE: Lazy<CacheManager> =
            Lazy::new(|| CacheManager::new(PathBuf::from("").as_path(), CacheMode::None));

        pub const TEST_POOL: Lazy<ThreadPool> = Lazy::new(|| {
            let threads = num_cpus::get() + 1;
            ThreadPoolBuilder::new().num_threads(threads).build().unwrap()
        });

        /// The hashing algorithm for testing.
        pub const TEST_HASH_ALG: HashAlg = HashAlg::DoubleGradient;

        /// The default tolerance threshold for testing.
        pub const TEST_TOLERANCE_THRESHOLD: u32 = 10;

        /// Obtains the number of undirected edges for a graph with the given number of nodes.
        pub fn get_edge_count(nodes: usize) -> usize {
            nodes * (nodes - 1) / 2
        }
    }
}
