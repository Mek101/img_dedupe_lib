use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;

use crate::graphconstructor::imagedata::ImageData;
use crate::wrappers::filedata::FileData;

pub struct Job<'a> {
    pub item: &'a ImageData,
    pub comparables: &'a [ImageData],
}

pub struct JobProducer {
    data: Vec<ImageData>,
    index: AtomicUsize,
}

impl JobProducer {
    pub fn new<'a>(files: impl IntoIterator<Item = &'a Arc<FileData>> + Send) -> Self {
        Self {
            data: files.into_iter().map(|f| ImageData::new(f.to_owned())).collect(),
            index: AtomicUsize::new(0),
        }
    }

    pub fn count(&self) -> usize {
        self.data.len()
    }

    pub fn get_job(&self) -> Option<Job> {
        // Possible but unlikely overflow...
        let index = self.index.fetch_add(1, Ordering::Relaxed);

        if index < self.data.len() - 1 {
            Some(Job {
                item: &self.data[index],
                comparables: &self.data[index + 1..],
            })
        } else {
            None
        }
    }
}
