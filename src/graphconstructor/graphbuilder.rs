use std::sync::Mutex;

use crate::graphconstructor::BatchBuffer;
use crate::ImageGraph;

/// Data shared between the threads.
/// Contains and allows to access in a thread-safe manner to both the input and output data structures.
pub struct GraphBuilder {
    graph: Mutex<Box<ImageGraph>>,
}

impl GraphBuilder {
    fn get_edge_count(n: usize) -> usize {
        n * (n - 1) / 2
    }

    /// Inserts the filedata items in graph with the relative edges.
    fn push_batch(graph: &mut ImageGraph, batch: &BatchBuffer) {
        for (item, comp, dist) in batch {
            let item_index = item.insert_or_get_node(graph);
            let comp_index = comp.insert_or_get_node(graph);
            graph.update_edge(item_index, comp_index, dist.to_owned());
        }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            graph: Mutex::new(Box::new(ImageGraph::with_capacity(capacity, GraphBuilder::get_edge_count(capacity)))),
        }
    }

    /// Obtains the final output graph, consuming self in order to limit further access to the graph.
    pub fn get_result(self) -> Box<ImageGraph> {
        let graph = self.graph.lock().unwrap();
        graph.to_owned()
    }

    /// Inserts a batch of items connected to a given item.
    /// Always tries to obtain the lock.
    pub fn flush_batch(&self, batch: &BatchBuffer) {
        if !batch.is_empty() {
            let mut graph = self.graph.lock().unwrap();
            GraphBuilder::push_batch(&mut graph, batch);
        }
    }

    /// If the lock currently is free, inserts a batch of items connected to a given item.
    /// Locks only if the lock is already free.
    pub fn try_flush_batch(&self, batch: &BatchBuffer) -> bool {
        if !batch.is_empty() {
            if let Ok(mut graph) = self.graph.try_lock() {
                GraphBuilder::push_batch(&mut graph, batch);
                true
            } else {
                false
            }
        } else {
            true
        }
    }
}
