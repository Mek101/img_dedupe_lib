mod graphbuilder;
mod imagedata;
mod jobproducer;

use std::cmp;
use std::sync::Arc;

use img_hash::{HashAlg, Hasher, HasherConfig};
use rayon::ThreadPool;

use crate::graphconstructor::graphbuilder::GraphBuilder;
use crate::graphconstructor::imagedata::ImageData;
use crate::graphconstructor::jobproducer::JobProducer;
use crate::wrappers::cachemanager::{CacheConnector, CacheManager};
use crate::wrappers::filedata::FileData;
use crate::wrappers::Distance;
use crate::ImageGraph;

type BatchBuffer<'a> = Vec<(&'a ImageData, &'a ImageData, Distance)>;

const BATCH_SIZE: usize = 128;

// Builds a graph in the context.
fn build_graph_on_thread<'a>(producer: &JobProducer, builder: &GraphBuilder, hasher: &Hasher, cache: &CacheConnector) {
    // Splitting the process in batches in order not to occupy the blocked calls for too long.
    // Pre-allocates a batch container to re-use after each iteration.
    let mut buffer = BatchBuffer::with_capacity(BATCH_SIZE);

    while let Some(job) = producer.get_job() {
        let item = job.item;

        for comp in job.comparables {
            // Potentially heavy calculations and locking.
            let dist = item.get_distance(&comp, &hasher, &cache);

            buffer.push((&item, &comp, dist));

            // Attempts a flush when the batch is full.
            if buffer.len() >= BATCH_SIZE {
                builder.try_flush_batch(&buffer);
            }
        }
    }

    // Ensures the batch has been flushed.
    builder.flush_batch(&buffer)
}

/// Builds the graph using a pool of threads.
pub fn build_graph<'a>(
    files: impl IntoIterator<Item = &'a Arc<FileData>> + Send,
    alg: HashAlg,
    cache_manager: &CacheManager,
    pool: &ThreadPool,
) -> Box<ImageGraph> {
    let producer = JobProducer::new(files);
    let builder = GraphBuilder::with_capacity(producer.count());

    let parallelism = cmp::min(pool.current_num_threads(), producer.count());

    pool.scope(|scope| {
        for _ in 0..parallelism {
            scope.spawn(|_| {
                let hasher = HasherConfig::new().hash_alg(alg).to_hasher();
                let connector = cache_manager.get_cache_connection();

                build_graph_on_thread(&producer, &builder, &hasher, &connector);
            });
        }
    });

    builder.get_result()
}

#[cfg(test)]
mod tests {
    use crate::tests::utils::{self, TEST_CACHE_MANAGER_NONE, TEST_FILEDATA, TEST_HASH_ALG, TEST_POOL};

    /// Build a graph on multiple cores.
    /// Tests if the resulting graph has the correct number of edges and nodes.
    #[test]
    fn build_graph() {
        let node_count = TEST_FILEDATA.len();
        let edge_count = utils::get_edge_count(node_count);

        let graph = super::build_graph(TEST_FILEDATA.iter(), TEST_HASH_ALG, &TEST_CACHE_MANAGER_NONE, &TEST_POOL);

        let node_count_res = graph.node_count();
        assert_eq!(node_count, node_count_res, "Wrong node count: expected {} found {}!", node_count, node_count_res);
        let edge_count_res = graph.edge_count();
        assert_eq!(edge_count, edge_count_res, "Wrong edge count: expected {} found {}!", edge_count, edge_count_res);
    }
}
