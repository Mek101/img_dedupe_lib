use std::fs;
use std::path::{Path, PathBuf};

use fasthash::xx;
use img_hash::ImageHash;
use once_cell::sync::OnceCell;
use os_str_bytes::OsStrBytes;
use sled::Db;

use crate::wrappers::filedata::FileData;

/// How and if the cache is used.
#[derive(Copy, Clone)]
pub enum CacheMode {
    /// Don't use the cache.
    None,
    /// Don't update the cache.onditional compilation block
    ReadOnly,
    /// Use and update the cache.
    ReadWrite,
}

/// Owner and mode determiner of the cache. All connections to the cache in a given session must originate from a
/// CacheManager.
pub struct CacheManager {
    db_path: PathBuf,
    mode: CacheMode,
    db: OnceCell<Db>,
}

/// Represents a thread-local connection to the cache. Enables a thread-safe manner to query and update the cache.
pub struct CacheConnector<'a> {
    manager: &'a CacheManager,
}

impl CacheManager {
    fn get_db(&self) -> &Db {
        self.db.get_or_init(|| sled::open(self.db_path.as_path()).unwrap())
    }

    pub fn new(path: &Path, mode: CacheMode) -> Self {
        CacheManager {
            db_path: path.to_owned(),
            mode,
            db: OnceCell::<Db>::new(),
        }
    }

    pub fn get_cache_connection(&self) -> CacheConnector {
        CacheConnector {
            manager: &self,
        }
    }
}

impl<'a> CacheConnector<'a> {
    fn get_key(path: &Path) -> [u8; 8] {
        // Can handle different path casing on windows.
        let path_canonized = fs::canonicalize(path).unwrap();
        let bytes = path_canonized.to_bytes();
        xx::hash64(bytes).to_ne_bytes()
    }

    pub fn get_signature(&self, file: &FileData) -> Option<ImageHash> {
        match self.manager.mode {
            CacheMode::None => None,
            CacheMode::ReadOnly | CacheMode::ReadWrite => {
                let key = CacheConnector::get_key(file.path());
                let bytes = self.manager.get_db().get(key).ok().flatten()?.to_vec();
                Some(ImageHash::from_base64(&String::from_utf8(bytes).unwrap()).unwrap())
            }
        }
    }

    pub fn set_signature(&self, file: &FileData, signature: &ImageHash) {
        match self.manager.mode {
            CacheMode::None | CacheMode::ReadOnly => (),
            CacheMode::ReadWrite => {
                let key = CacheConnector::get_key(file.path());
                self.manager.get_db().insert(key, signature.to_base64().into_bytes()).unwrap();
            }
        }
    }
}
