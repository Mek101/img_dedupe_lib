use std::fs::File;
use std::io::BufReader;
use std::path::{Path, PathBuf};

use image::{io::Reader, DynamicImage, GenericImageView, ImageFormat};

pub struct FileData {
    path: PathBuf,
    format: ImageFormat,
    image_impl: Box<DynamicImage>,
}

impl FileData {
    // Using a reader instead of `image::open` in order to manage images with wrong or no extensions.
    fn get_reader(path: &Path) -> Reader<BufReader<File>> {
        Reader::open(path).unwrap().with_guessed_format().unwrap()
    }

    pub fn new(path: &Path, image: DynamicImage) -> Self {
        let reader = FileData::get_reader(path);
        Self {
            path: path.to_owned(),
            format: reader.format().unwrap(),
            image_impl: Box::new(image),
        }
    }

    pub fn from_path(path: &Path) -> Self {
        let reader = FileData::get_reader(path);
        Self {
            path: path.to_owned(),
            format: reader.format().unwrap(),
            image_impl: Box::new(reader.decode().unwrap()),
        }
    }

    pub(crate) fn image_impl(&self) -> &DynamicImage {
        &self.image_impl
    }

    pub fn path(&self) -> &Path {
        self.path.as_path()
    }

    pub fn dimensions(&self) -> (u32, u32) {
        self.image_impl.dimensions()
    }

    pub fn height(&self) -> u32 {
        self.image_impl.height()
    }

    pub fn width(&self) -> u32 {
        self.image_impl.width()
    }

    pub fn depth(&self) -> u32 {
        match *self.image_impl {
            DynamicImage::ImageLuma8(_) => 1,
            DynamicImage::ImageLumaA8(_) => 2,
            DynamicImage::ImageRgb8(_) => 3,
            DynamicImage::ImageRgba8(_) => 4,
            DynamicImage::ImageBgr8(_) => 3,
            DynamicImage::ImageBgra8(_) => 4,
            DynamicImage::ImageLuma16(_) => 2,
            DynamicImage::ImageLumaA16(_) => 4,
            DynamicImage::ImageRgb16(_) => 6,
            DynamicImage::ImageRgba16(_) => 8,
        }
    }

    pub fn format(&self) -> ImageFormat {
        self.format
    }
}

impl ToOwned for FileData {
    type Owned = FileData;

    fn to_owned(&self) -> Self::Owned {
        Self::Owned {
            path: self.path.to_owned(),
            format: self.format,
            image_impl: self.image_impl.clone(),
        }
    }
}
