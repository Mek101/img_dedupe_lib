use std::sync::Arc;

use petgraph::{algo, graph::NodeIndex, Graph, Undirected};
use rayon::{prelude::*, ThreadPool};

use crate::wrappers::filedata::FileData;
use crate::ImageGraph;

/// Collects all the image components with an edge weight bellow the given threshold
pub fn split_to_groups(
    graph: &ImageGraph,
    tolerance_threshold: u32,
    discard_unique: bool,
    pool: &ThreadPool,
) -> Vec<Vec<Arc<FileData>>> {
    // All operations are done in the given threadpool.
    pool.install(|| {
        // Filters out edges with a weight under the tollerance threshold.
        let filtered_edges: Vec<(NodeIndex, NodeIndex)> = graph
            .raw_edges()
            .par_iter()
            .filter_map(|e| {
                if e.weight <= tolerance_threshold {
                    Some((e.source(), e.target()))
                } else {
                    None
                }
            })
            .collect();

        // A shadow copy of the original graph with the same edges but no weights on the nodes.
        // Since the edges to unique nodes have been cut, those nodes aren't present in the shadow graph.
        let shadow_graph = Graph::<(), u32, Undirected>::from_edges(filtered_edges);

        // Partitions the shadow graph into strongly connected components.
        let mut connected_components = algo::tarjan_scc(&shadow_graph);

        if !discard_unique {
            // Collect every node from the original graph absent from the shadow graph.
            let unique_nodes: Vec<Vec<NodeIndex>> = graph
                .node_indices()
                .par_bridge()
                .filter_map(|node_index| {
                    if !shadow_graph.node_indices().any(|shadow_index| shadow_index == node_index) {
                        Some(vec![node_index])
                    } else {
                        None
                    }
                })
                .collect();

            // Add the unique node components.
            connected_components.extend(unique_nodes);
        }

        // Collects into groups the weights from the original graph mapped from the shadow graph.
        connected_components
            .par_iter()
            .map(|component| {
                let mut mapped_group: Vec<Arc<FileData>> =
                    component.iter().map(|node_index| graph[*node_index].clone()).collect();
                mapped_group.shrink_to_fit();
                mapped_group
            })
            .collect()
    })
}

/// Ranks in descending order the given image groups with the given predicates.
pub fn rank<P>(groups: &[Vec<Arc<FileData>>], predicates: &[P], pool: &ThreadPool) -> Vec<Vec<(Arc<FileData>, i32)>>
where
    P: Sync + Fn(&FileData) -> i32 + Send,
{
    // All operations are done in the given threadpool.
    pool.install(|| {
        groups.par_iter().map(|g| {
            let mut g_ranked: Vec<(Arc<FileData>, i32)> = g
                .iter()
                .map(|f| {
                    // Accumulate points from each predicate.
                    let points = predicates.iter().fold(0, |acc, p| acc + p(f.as_ref()));
                    (f.clone(), points)
                })
                .collect();
            // Sorts in descending order.
            g_ranked.sort_by_key(|t| -t.1);
            g_ranked.shrink_to_fit();
            g_ranked
        })
    })
    .collect()
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;
    use std::path::PathBuf;
    use std::sync::Arc;

    use once_cell::sync::Lazy;
    use petgraph::graph::NodeIndex;

    use crate::tests::utils::{
        self, TEST_FILEDATA, TEST_IMAGES_CORRECT_MATCHES, TEST_IMAGES_UNIQUE_COUNT, TEST_POOL, TEST_TOLERANCE_THRESHOLD,
    };
    use crate::wrappers::filedata::FileData;
    use crate::ImageGraph;

    /// A fully-connected graph with the correct groups linked by edges withing the tollerance threshold.
    const TEST_GRAPH: Lazy<ImageGraph> = Lazy::new(|| {
        let node_count = TEST_FILEDATA.len();
        let edge_count = utils::get_edge_count(node_count);

        let mut graph = ImageGraph::with_capacity(node_count, edge_count);

        for group in TEST_IMAGES_CORRECT_MATCHES.iter() {
            let group_indexes: Vec<NodeIndex> =
                group.iter().map(|img_path| graph.add_node(Arc::new(FileData::from_path(img_path)))).collect();

            // Connects each node of group to the others in the same group with an edge within the tollerance threshold.
            for &i in group_indexes.iter() {
                for &other in group_indexes.iter() {
                    if i != other {
                        graph.add_edge(i, other, TEST_TOLERANCE_THRESHOLD / 2);
                    }
                }
            }
        }

        // Connects each node with every other node it isn't already connected with (ie, outside it's group) with and
        // edge outside the tollerance threshold.
        for i in graph.node_indices() {
            for other in graph.node_indices() {
                if i != other && graph.contains_edge(i, other) {
                    graph.add_edge(i, other, TEST_TOLERANCE_THRESHOLD * 2);
                }
            }
        }
        graph
    });

    /// Hashes of the correct groups.
    const TEST_IMAGES_MATCHES_HASH: Lazy<Vec<HashSet<PathBuf>>> = Lazy::new(|| {
        TEST_IMAGES_CORRECT_MATCHES
            .iter()
            .map(|g| {
                let hash: HashSet<_> = g.iter().map(|p| p.to_path_buf()).collect();
                hash
            })
            .collect()
    });

    /// Hashes the given input and compares it with the correct matches. Returns true if it matches any of them.
    fn matches_any<'a>(other: impl Iterator<Item = PathBuf>) -> bool {
        let other: HashSet<PathBuf> = other.collect();

        TEST_IMAGES_MATCHES_HASH.iter().any(|m| *m == other)
    }

    /// Checks if all the groups match.
    fn match_all(groups: impl IntoIterator<Item = Vec<Arc<FileData>>>) -> bool {
        groups.into_iter().all(|g_res| matches_any(g_res.into_iter().map(|f| f.path().to_path_buf())))
    }

    #[test]
    fn split_graph_no_unique() {
        let groups = super::split_to_groups(&TEST_GRAPH, TEST_TOLERANCE_THRESHOLD, true, &TEST_POOL);

        // The number of groups must match with the unique images/groups excluded.
        let expected = TEST_IMAGES_CORRECT_MATCHES.len() - TEST_IMAGES_UNIQUE_COUNT;
        let res = groups.len();
        assert_eq!(expected, res, "Wrong groups count: expected {}, found {}!", expected, res);

        assert!(match_all(groups), "Groups not matching with correct groups!");
    }

    #[test]
    fn split_graph_with_unique() {
        let groups = super::split_to_groups(&TEST_GRAPH, TEST_TOLERANCE_THRESHOLD, false, &TEST_POOL);

        // The number of groups must match.
        let expected = TEST_IMAGES_CORRECT_MATCHES.len();
        let res = groups.len();
        assert_eq!(expected, res, "Wrong groups count: expected {}, found {}!", expected, res);

        assert!(match_all(groups), "Groups not matching with correct groups!");
    }
}
